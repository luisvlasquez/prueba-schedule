#forms.py
from django import forms  
from .models import Contact
class ContactForm(forms.ModelForm):  
    class Meta:  
        model = Contact  
        fields = ['name','last_name','phone','email','photo']
        widgets = { 'name': forms.TextInput(attrs={ 'class': 'form-control' }), 
            'last_name': forms.TextInput(attrs={ 'class': 'form-control' }),
            'email': forms.EmailInput(attrs={ 'class': 'form-control' }),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'onkeypress':'return event.charCode >= 48 && event.charCode <= 57'} ),
        }
