from django.shortcuts import render,redirect
from django.views.generic.edit import UpdateView, DeleteView
#models
from .models import Contact

#form 
from .forms import ContactForm
# Create your views here.

def index(request):
    contact = Contact.objects.all().order_by('name')
    return render(request,"contact/index.html",{'contacts':contact})  

def addnew(request):  
    if request.method == "POST":  
        form = ContactForm(request.POST, request.FILES)  
        if form.is_valid():  
          
            try:  
                form.save()  
                return redirect('/')  
            except:  
                pass  
    else:  
        form = ContactForm()  
    return render(request,'contact/add.html',{'form':form})  

def edit(request, pk):  
    contact = Contact.objects.get(id=pk)  
    return render(request,'contact/edit.html', {'contact':contact})  
def update(request, pk):  
    contact = Contact.objects.get(id=pk)  
    form = ContactForm(request.POST,request.FILES ,instance = contact)  
    if form.is_valid():  
        form.save()  
        return redirect("/")  
    return render(request, 'contact/edit.html', {'contact': contact, 'form':form})  
def destroy(request, pk):  
    contact = Contact.objects.get(id=pk)  
    contact.delete()  
    return redirect("/")  