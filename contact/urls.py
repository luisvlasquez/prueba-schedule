#urls.py
from django.urls import path  
from .views import index ,addnew, edit,update,destroy
urlpatterns = [  

    path('', view =index),  
    path('addnew', view=addnew),
    path('edit/<int:pk>', edit),  
    path('update/<int:pk>', update),  
    path('delete/<int:pk>', destroy),   
]  