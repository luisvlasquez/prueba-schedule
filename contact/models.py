#django
from django.db import models
from django.core.validators import RegexValidator

# Create your models here.
class Contact(models.Model):
    phone_regex = RegexValidator(
        regex=r'^[1-9]{1}[0-9]{1,9}$',     
        message=('Solo se permiten numeros y debe tener minimo 2 numeros y maximo 10') # 
    )
    name = models.CharField(max_length= 50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    phone = models.CharField(max_length=10, validators= [phone_regex],unique=True, error_messages={'unique':"ya existe este numero de telefono"})
    photo = models.ImageField(blank=True,null=True,upload_to='images')

    def __str__(self):
        return self.name